<?php

define("EARTH_RADIUS_IN_KM", 6373);

// Semi-axes of WGS-84 geoidal reference
define('WGS84_a', 6378137.0);  // Major semiaxis [m]
define('WGS84_b', 6356752.3);  // Minor semiaxis [m]


class GeocodeUtil {
	private static function degree2Radiant($deg) {
		return  $deg * M_PI/180.0;
	}
	
	private static function radiant2Degree($rad) {
		return   180.0 * $rad/M_PI;
	}
	
	public static function earthRadiusWGS84($lat) {
	    // http://en.wikipedia.org/wiki/Earth_radius
	    $An = WGS84_a*WGS84_a * cos($lat);
	    $Bn = WGS84_b*WGS84_b * sin($lat);
	    $Ad = WGS84_a * cos($lat);
	    $Bd = WGS84_b * sin($lat);
	    return sqrt( ($An*$An + $Bn*$Bn) / ($Ad*$Ad + $Bd*$Bd) );
	}

	
	public static function distanceInKm($lat1, $lon1, $lat2, $lon2) {
		$radiusInMeter = EARTH_RADIUS_IN_KM;

		// convert to radiant
		$lat1 = self::degree2Radiant($lat1);
		$lon1 = self::degree2Radiant($lon1);
		$lat2 = self::degree2Radiant($lat2);
		$lon2 = self::degree2Radiant($lon2);
		
		$dlat = $lat2 - $lat1;
		$dlon = $lon2 - $lon1;

		$a = pow(sin($dlat/2), 2) + cos($lat1) * cos($lat2) * pow(sin($dlon/2), 2);
		$c = 2 * atan2( sqrt($a), sqrt(1-$a) );

		$d = $radiusInMeter * $c;

		return $d;
	}
	
	
	public static function calculateGeoBoundingBox($lat, $lon, $halfSideInKm, &$minLat, &$maxLat, &$minLon, &$maxLon) {
	    $lat = self::degree2Radiant($lat);
	    $lon = self::degree2Radiant($lon);
	    $halfSide = 1000 * $halfSideInKm;

	    # Radius of Earth at given latitude
	    $earthRadius = self::earthRadiusWGS84($lat);
	    # Radius of the parallel at given latitude
	    $pEarthRadius = $earthRadius * cos($lat);

	    $minLat = self::radiant2Degree($lat - $halfSide/$earthRadius);
	    $maxLat = self::radiant2Degree($lat + $halfSide/$earthRadius);
	    $minLon = self::radiant2Degree($lon - $halfSide/$pEarthRadius);
	    $maxLon = self::radiant2Degree($lon + $halfSide/$pEarthRadius);
	}
	
}

?>