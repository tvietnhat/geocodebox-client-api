<?php

define('MILE_TO_KM_RATIO', 1.609344);
define('MILE_TO_FOOT_RATIO', 5280);
define('METER_TO_FOOT_RATIO', 3.28084);

class MeasurementUtil {
	public static function mile2Kilometer($miles) {
		return $miles * MILE_TO_KM_RATIO;
	}
	
	public static function kilometer2Mile($kms) {
		return $kms / MILE_TO_KM_RATIO;
	}
	
	public static function mile2Foot($miles) {
		return $miles * MILE_TO_FOOT_RATIO;
	}
	
	public static function foot2Mile($feet) {
		return $feet / MILE_TO_FOOT_RATIO;
	}
	
	public static function meter2Foot($meters) {
		return $meters * METER_TO_FOOT_RATIO;
	}
	
	public static function foot2Meter($feet) {
		return $feet / METER_TO_FOOT_RATIO;
	}
	
	public static function degree2Radiant($deg) {
		return  $deg * M_PI/180.0;
	}
	
	public static function radiant2Degree($rad) {
		return   180.0 * $rad/M_PI;
	}
}
?>