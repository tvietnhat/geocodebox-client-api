<?php

require_once 'app_modules/geo-object.php';
require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	$error = null;
	
	if ($authInfo != null) {
		$searchParams = $_GET;
		GeoObject::searchObjectLocation($searchParams, function ($result, $searchError) use (&$responseObj) {
			if ($searchError == null) {
				$responseObj = ($result != null) ? $result : [];
			} else {
				$responseObj = [ 'error' => $searchError];
			}
		});
	} else {
		$error = $authError;
	}
	
	if ($error != null) {
		$responseObj = [ 'error' => $error];
	}
	
	header('Content-Type: application/json');
	echo json_encode($responseObj);
});



?>
