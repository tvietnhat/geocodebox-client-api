<?php

require_once 'app_modules/geo-object.php';
require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	$error = null;
	global $g_AppErrors;
	
	if ($authInfo != null) {
		$objectId = array_key_exists('object_id', $_GET) ? $_GET['object_id'] : null;
		if ($objectId == null || !GeoObject::validateObjectId($objectId)) {
			$error = $g_AppErrors['MISSING_OR_INVALID_SUBJECT_ID'];
		} else {
			$object = json_decode(file_get_contents("php://input"), true);
			$error = GeoObject::validateObjectForUpdate($object);
			if ($error == null) {
				// do the update
				GeoObject::updateObject($objectId, $object);
				$responseObj = [ 'success' => true ];
			}
		}
	} else {
		$error = $authError;
	}
	
	if ($error != null) {
		$responseObj = [ 'error' => $error];
	}
	
	header('Content-Type: application/json');
	echo json_encode($responseObj);
});



?>
