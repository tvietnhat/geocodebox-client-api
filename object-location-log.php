<?php

require_once 'app_modules/geo-object.php';
require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	$error = null;
	global $g_AppErrors;
	
	if (null != $authInfo) {
		$objectId = array_key_exists('object_id', $_GET) ? $_GET['object_id'] : null;
		if (null == $objectId || !GeoObject::validateObjectId($objectId)) {
			$error = $g_AppErrors['MISSING_OR_INVALID_SUBJECT_ID'];
		} else {
			$locations = json_decode(file_get_contents("php://input"), true);
			if (null != $locations && count($locations) > 0) {
				if (array_key_exists(0, $locations) && array_key_exists(count($locations) - 1, $locations)) {
					// multiple location records
					foreach ($locations as $loc) {
						$error = GeoObject::validateObjectLocation($loc);
						if (null != $error) {
							break;
						}
					}
					if (null == $error) {
						GeoObject::addObjectLocationArray($objectId, $locations);
						$responseObj = [ 'success' => true ];
					}
				} else {
					// Only a single location record
					$error = GeoObject::validateObjectLocation($locations);
					if (null == $error) {
						GeoObject::addObjectLocation($objectId, $locations);
						$responseObj = [ 'success' => true ];
					}
				}
			} else {
				$error = $g_AppErrors['UNKNOWN_ERROR'];
			}
		}
	} else {
		$error = $authError;
	}
	
	if (null != $error) {
		$responseObj = [ 'error' => $error];
	}

	header('Content-Type: application/json');
	echo json_encode($responseObj);
});

	
?>
