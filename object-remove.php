<?php

// Make sure this is a POST request
if ( $_SERVER['REQUEST_METHOD'] !== 'POST' ) {
	$sapi_type = php_sapi_name();
	if (substr($sapi_type, 0, 3) == 'cgi') {
	    header("Status: 403 Forbidden");
	} else {
	    header("HTTP/1.1 403 Forbidden");
	}
	die;
}


require_once 'app_modules/geo-object.php';
require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	$error = null;
	
	if ($authInfo != null) {
		$objectId = array_key_exists('object_id', $_GET) ? $_GET['object_id'] : null;
		if ($objectId == null || !GeoObject::validateObjectId($objectId)) {
			$error = $g_AppErrors['MISSING_OR_INVALID_SUBJECT_ID'];
		} else {
			GeoObject::removeObjectLocationHistory($objectId);
			GeoObject::removeObjectTags($objectId);
			GeoObject::removeObject($objectId);
			$responseObj = [ 'success' => true ];
		}
	} else {
		$error = $authError;
	}
	
	if ($error != null) {
		$responseObj = [ 'error' => $error];
	}
	
	header('Content-Type: application/json');
	echo json_encode($responseObj);
});



?>
