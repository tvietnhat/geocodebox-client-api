<?php

require_once 'app_modules/geo-object.php';
require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	$error = null;
	global $g_AppErrors;
	
	if ($authInfo != null) {
		$objectId = array_key_exists('object_id', $_GET) ? $_GET['object_id'] : null;
		if ($objectId == null || !GeoObject::validateObjectId($objectId)) {
			$error = $g_AppErrors['MISSING_OR_INVALID_SUBJECT_ID'];
		} else {
			$object = GeoObject::findById($objectId);
			if ($object == null) {
				$error = $g_AppErrors['SUBJECT_NOT_FOUND'];
			} else {
				$responseObj = $object;
			}
		}
	} else {
		$error = $authError;
	}
	
	if ($error != null) {
		$responseObj = [ 'error' => $error];
	}

	header('Content-Type: application/json');
	echo json_encode($responseObj);
});

	
?>
