<?php

require_once 'app_modules/geo-object.php';
require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	
	if ($authInfo != null) {
		$searchParams = $_GET;
		GeoObject::searchObject($searchParams, function ($result, $searchError) use (&$responseObj) {
			if ($searchError == null) {
				$responseObj = ($result != null) ? $result : [];
			} else {
				$responseObj = [ 'error' => $searchError];
			}
		});
	} else {
		$responseObj = [ 'error' => $authError];
	}
	
	header('Content-Type: application/json');
	echo json_encode($responseObj);
});

	
?>
