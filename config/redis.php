<?php

return [
	'CACHE' => [
		'host'     => '127.0.0.1',
		'port'     => 6379
	],
	'PUBSUB' => [
		'host'     => '127.0.0.1',
		'port'     => 6379
	],
];

?>
