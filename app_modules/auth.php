<?php

require_once ( __DIR__ . '/cache.php' );
require_once ( __DIR__ . '/database.php' );
require_once ( dirname(__DIR__) . '/libs/GUID/class.php' );
require_once ( __DIR__ . '/error.php' );
require_once ( __DIR__ . '/pubsub.php' );


define("ACCOUNT_AUTH_EXPIRATION", 3600 * 24 * 60); // 60 days
// define("ACCOUNT_AUTH_INFO_IGNORE_REFRESH_PERIOD", 60 * 1000);// Don't refresh authentication info again with within 1 minute since last

class AppAuth {

	protected static function _findAccountByLoginName($loginName) {
		$row = AppDB::$adminDb->queryFirstRow("SELECT id, login_name, locked, password_hash, first_name, last_name FROM user_account WHERE login_name=%?", $loginName);
		
		return $row;
	}
	
	public static function authenticate($username, $password, $apiKey, $done) {
		// check if it the user is authenticated
		$cacheKey = 'auth_info:' . $username . ':' . $apiKey;
		$cache = AppCache::$authCache;
		$authInfo = $cache->retrieve($cacheKey);
		// if ($authInfo) {
		// 	$now = time();
		// 	$intervalInMiliseconds = $now - $authInfo['timestamp'];
		// 	if ($intervalInMiliseconds < ACCOUNT_AUTH_INFO_IGNORE_REFRESH_PERIOD) {
		// 		return $done($authInfo, null);
		// 	}
		// }
	
		$account = self::_findAccountByLoginName($username);

		$isValid = true;
		$authError = null;
		global $g_AppErrors;
	
		if ($isValid && $account == null) {
			$isValid = false;
		    $authError = $g_AppErrors['INVALID_USERNAME_OR_PASSWORD'];
		}

		// check if account is locked
		if ($isValid && $account['locked'] == 1) {
			$isValid = false;
		    $authError = $g_AppErrors['USER_ACCOUNT_LOCKED'];
		}

		// check password
		if ($isValid && $account) {
			$pwHash = sha1($password);
			if ($account['password_hash'] !== $pwHash) {
				$isValid = false;
			    $authError = $g_AppErrors['INVALID_USERNAME_OR_PASSWORD'];
			}
		}
		
		// check API Key
		$project = AppDB::$adminDb->queryFirstRow("SELECT id, object_tracking_enabled,
		database_config, objects_limit, object_tracking_record_limit FROM user_project WHERE status='active' AND user_account_id=%? AND api_key=%?", $account['id'], $apiKey);
		if ($project == null) {
			$isValid = false;
		    $authError = $g_AppErrors['INVALID_API_KEY'];
		}

		if ($isValid) {
			// generate token
			$token = sha1(GUID::get() . $apiKey);
			// blend the SHA1 hash with first 8 characters of the API key
			for($i = 0; $i < 8; $i++) {
				$token = substr_replace($token, substr($apiKey, $i, 1), $i*2 + 1, 0);
			}
			$authInfo = [ 
				'token' => $token, 
				'timestamp' => time(),
				'accountId'=> $account['id'],
				'project' => [
					'id' => $project['id'],
					'object_tracking_enabled' => ($project['object_tracking_enabled'] == 1 ? true : false),
					'database_config' => json_decode($project['database_config'], true),
					'objects_limit' => $project['objects_limit'],
					'object_tracking_record_limit' => $project['object_tracking_record_limit']
				]
			];

			// cache the auth info for 60 seconds
			$cache->store($cacheKey, $authInfo, ACCOUNT_AUTH_EXPIRATION);
			$cache->store('token:' . $token, $cacheKey, ACCOUNT_AUTH_EXPIRATION);
		
			AppDB::initUserDbWithConfig($authInfo['project']['database_config']);
		
		    return $done($authInfo, null);
		} else {
			// TODO: log fail attemps
		    return $done(null, $authError);
		}
	}
	
	public static function authenticateRequest($done) {
		$authenticated = false;
		global $g_AppErrors;
		
		// check if it the user is authenticated
		$token = array_key_exists('access_token', $_GET) ? $_GET['access_token'] : null;
	
		if ($token != null) {
			// remove white spaces
			$token = trim($token);
		
			$cache = AppCache::$authCache;
			$authInfoKey = $cache->retrieve('token:' . $token);
			$authInfo = $cache->retrieve($authInfoKey);
			if ($authInfo) {
				$authenticated = true;
			}
		}
		
		if ($authenticated) {
			AppDB::initUserDbWithConfig($authInfo['project']['database_config']);
			return $done($authInfo, null);
		} else {
			return $done(null, $g_AppErrors['MISSING_OR_INVALID_ACCESS_TOKEN']);
		}
	}
}

?>
