<?php

require_once ( __DIR__ . '/database.php' );
require_once ( __DIR__ . '/error.php' );
require_once ( __DIR__ . '/pubsub.php' );
require_once ( dirname(dirname(__FILE__)) . '/libs/GeocodeUtil/class.php' );
require_once ( dirname(dirname(__FILE__)) . '/libs/Measurement/class.php' );


define("GEO_SUBJECT_SEARCH_RESULT_PAGE_SIZE_DEFAULT", 30);
define("GEO_SUBJECT_SEARCH_RESULT_PAGE_SIZE_MIN", 5);
define("GEO_SUBJECT_SEARCH_RESULT_PAGE_SIZE_MAX", 100);
define("GEO_SUBJECT_SEARCH_NEARBY_RESULT_LIMIT_MAX", 100);
define("GEO_SUBJECT_SEARCH_NEARBY_RESULT_LIMIT_DEFAULT", 20);
define('GEO_SUBJECT_LOCATION_SEARCH_RESULT_PAGE_SIZE_DEFAULt', 10);
define('GEO_SUBJECT_LOCATION_SEARCH_RESULT_PAGE_SIZE_MAX', 100);
define('GEO_SUBJECT_GEOCODE_SEARCH_MULTIPLIER', 1000000000000);


class GeoObject {
	/* Validate object from HTTP request */
	static function validateObjectId($id) {
		return (is_string($id) && strlen($id) > 0 && strlen($id) <= 36);
	}

	static function validateObjectName($name) {
		return (is_string($name) && strlen($name) > 0 && strlen($name) <= 64);
	}

	static function validateObjectDescription($description) {
		return (is_string($description) && strlen($description) > 0 && strlen($description) <= 255);
	}

	static function validateObjectStatus($status) {
		return (is_string($status) && strlen($status) <= 45);
	}

	static function validateObjectCustomData($custom_data) {
		return (is_string($custom_data) && strlen($custom_data) <= 1024);
	}

	static function validateObjectTags($tags) {
		$valid = true;
		
		if (!is_string($tags) || strlen($tags) > 450) {
			$valid = false;
		}
		
		if ($valid) {
			$keywords = explode(',', $tags);
			if (count($keywords) > 10) {
				$valid = false;
			}
			
			if($valid) {
				// validate keyword's length
				foreach ($keywords as $keyword) {
					if (strlen($keyword) > 45) {
						$valid = false;
						break;
					}
				}
			}
			
			if ($valid) {
				// validate duplicate values
				$uniqueKeywords = array_unique($keywords);
				if (count($uniqueKeywords) != count($keywords)) {
					// duplicate value exists
					$valid = false;
				}
			}
		}
		return $valid;
	}

	static function validateObjectLat($lat) {
		return (is_numeric($lat) && $lat >= -90.0 && $lat <= 90.0);
	}

	static function validateObjectLon($lon) {
		return (is_numeric($lon) && $lon >= -180.0 && $lon <= 180.0);
	}

	static function validateObjectInvisible($invisible) {
		return (is_bool($invisible));
	}

	static function validateObjectContext($context) {
		return (is_string($context) && strlen($context) <= 255);
	}

	static function parseDistanceString($distance, &$unit, &$value) {
		$distance_unit = null;
		$distance_value = null;
		if (substr($distance, -2) == 'km' || substr($distance, -2) == 'ft' || substr($distance, -2) == 'mi') {
			$distance_unit =  substr($distance, -2);
			$distance_value =  substr($distance, 0, strlen($distance) - 2);
		} else if (substr($distance, -1) == 'm') {
			$distance_unit =  substr($distance, -1);
			$distance_value =  substr($distance, 0, strlen($distance) - 1);
		}
		$unit = $distance_unit;
		$value = $distance_value;
	}
	
	static function validateDistance($distance) {
		if (is_string($distance) && strlen($distance) > 0) {
			$distance_unit = null;
			$distance_value = null;
			self::parseDistanceString($distance, $distance_unit, $distance_value);
			if ($distance_unit != null && is_numeric($distance_value)) {
				return true;
			}
		}
		return false;
	}

	static function dateTimeFromISO8601String($timeStr) {
		$timestamp = date_create_from_format('Y-m-d\TH:i:sP', $timeStr);
		return ($timestamp == false ? null : $timestamp) ;
	}

	static function validateObjectLocation($location) {
		global $g_AppErrors;
		if (array_key_exists('lat', $location) && $location['lat'] != null && !self::validateObjectLat($location['lat'])) {
			return $g_AppErrors['INVALID_LATITUDE'];
		}

		if (array_key_exists('lon', $location) && $location['lon'] != null && !self::validateObjectLon($location['lon'])) {
			return $g_AppErrors['INVALID_LONGITUDE'];
		}

		if (array_key_exists('timestamp', $location) && $location['timestamp'] != null && !self::dateTimeFromISO8601String($location['timestamp'])) {
			return $g_AppErrors['INVALID_LOCATION_DATA_TIMESTAMP'];
		}

		if (array_key_exists('speed', $location) && $location['speed'] != null && !is_numeric($location['speed'])) {
			return $g_AppErrors['INVALID_LOCATION_DATA_SPEED'];
		}

		if (array_key_exists('course', $location) && $location['course'] != null && !is_numeric($location['course'])) {
			return $g_AppErrors['INVALID_LOCATION_DATA_COURSE'];
		}

		if (array_key_exists('context', $location) && $location['context'] != null && !self::validateObjectContext($location['context'])) {
			return $g_AppErrors['INVALID_SUBJECT_LOCATION_CONTEXT'];
		}
	
		return null;
	}

	static function objectRowToJSON($row) {
		if ($row != null) {
			$object = [
				'id' => $row['id'], 
				'name' => $row['name'], 
				'description' => $row['description'], 
				'location_data' => [
					'lat' => $row['lat'] != null ? floatval($row['lat']) : null, 
					'lon' => $row['lon'] != null ? floatval($row['lon']) : null, 
					'speed' => $row['speed'] != null ? floatval($row['speed']) : null, 
					'course' => $row['course'] != null ? floatval($row['course']) : null, 
					'context' => $row['current_context'],
					'timestamp' => date('c', strtotime($row['location_data_timestamp']))
				],
				'tags' => $row['tags'], 
				'custom_data' => $row['custom_data'],
				'date_added' => date('c', strtotime($row['date_added'])), // convert to UTC-standard time string
				//'invisible' => ($firstRow['invisible'] != null && $firstRow['invisible'] != 0),
				'status' => $row['status'],
			];
			return $object;				
		} else {
			return null;
		}
	}
	

	////////////////////////////////////////////////////////////////////////////////////////////////
	/* Get object details. */
	public static function findById($objectId) {
		$firstRow = AppDB::$userDb->queryFirstRow('SELECT id, name, `description`, lat, lon, speed, course, tags, invisible, custom_data, location_data_timestamp, status, current_context, date_added FROM object WHERE id=%?', $objectId);
	
		return self::objectRowToJSON($firstRow);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	static function buildObjectSearchSQLQuery($searchParams, $done){
		$queryParams = [];
		$tags = array_key_exists('tags', $searchParams) && $searchParams['tags'] != null ? array_unique(explode(',', $searchParams['tags'])) : [];
		$useTagsParam = count($tags) > 0 ? true : false;
		
		$queryStatement = 'SELECT a.id, name, a.`description`, a.lat, a.lon, a.tags, a.invisible, a.custom_data, a.location_data_timestamp, a.status, a.current_context, a.speed, a.course, a.date_added FROM object a';

		// join object_tag if searching with tags
		if ($useTagsParam) {
			for ($i = 0; $i < count($tags); $i++) {
				$tag_tbl = 'tag_tbl_' . $i;
				$tag_param = 'tag_param_' . $i;
				$queryStatement .= ' INNER JOIN object_tag ' . $tag_tbl . ' ON a.id = ' . $tag_tbl .'.object_id AND ' . $tag_tbl . '.tag =%?_' . $tag_param;
				$queryParams[$tag_param] = $tags[$i];
			}
		}
		
		$queryStatement .= ' WHERE 1=1';
		
		// Adding geolocation query conditions
		if (array_key_exists('lat', $searchParams) && array_key_exists('lon', $searchParams) && array_key_exists('radius', $searchParams)) {
			$lat = $searchParams['lat'];
			$lon = $searchParams['lon'];
			$radius = $searchParams['radius'];
			$radius_unit = null;
			$radius_value = null;
			self::parseDistanceString($radius, $radius_unit, $radius_value);
			if ($radius_unit != null && is_numeric($radius_value)) {
				$radius_value = floatval($radius_value); // make sure the value is float/double
				if ($radius_unit == 'km') {
					$halfSideInKm = $radius_value;
				} else if ($radius_unit == 'm') {
					$halfSideInKm = $radius_value / 1000.0;
				} if ($radius_unit == 'mi') {
					$halfSideInKm = MeasurementUtil::mile2Kilometer($radius_value); // convert miles to kilometers
				} if ($radius_unit == 'ft') {
					$halfSideInKm = MeasurementUtil::foot2Meter($radius_value) / 1000.0; // convert feet to kilometers
				} else {
					// this shouldn't happen
				}
				$minLat = $maxLat = null; 
				$minLon = $maxLon = null;
				GeocodeUtil::calculateGeoBoundingBox($lat, $lon, $halfSideInKm, $minLat, $maxLat, $minLon, $maxLon);
				$queryStatement .= ' AND (a.lat_search BETWEEN %?_min_lat AND %?_max_lat) AND (a.lon_search BETWEEN %?_min_lon AND %?_max_lon)';
				$queryParams['min_lat'] = $minLat * GEO_SUBJECT_GEOCODE_SEARCH_MULTIPLIER;
				$queryParams['max_lat'] = $maxLat * GEO_SUBJECT_GEOCODE_SEARCH_MULTIPLIER;
				$queryParams['min_lon'] = $minLon * GEO_SUBJECT_GEOCODE_SEARCH_MULTIPLIER;
				$queryParams['max_lon'] = $maxLon * GEO_SUBJECT_GEOCODE_SEARCH_MULTIPLIER;
			}
			
		}
		
		// Adding 'name' parameter
		if (array_key_exists('name', $searchParams)) {
			$name = $searchParams['name'];
			if ( substr($name, strlen($name) - 1, 1) === '%' ) {
				$queryStatement .= ' AND a.name LIKE %?_name';
			} else {
				$queryStatement .= ' AND a.name=%?_name';
			}
			$queryParams['name'] = $name;
		}

		if (array_key_exists('status', $searchParams)) {
			$queryStatement .= ' AND a.status=%?_status';
			$queryParams['status'] = $searchParams['status'];
		}

		if (array_key_exists('result_limit', $searchParams)) {
			// Set the number of rows LIMIT for search nearby query
			$page = 1;
			$pageSize = GEO_SUBJECT_SEARCH_NEARBY_RESULT_LIMIT_MAX + 1;
		} else {
			$page = array_key_exists('page', $searchParams) ? intVal($searchParams['page']) : 1;
			$pageSize = array_key_exists('items_per_page', $searchParams) ? intVal($searchParams['items_per_page']) : GEO_SUBJECT_SEARCH_RESULT_PAGE_SIZE_DEFAULT;
		}

		if ($page == 1) {
			$queryStatement .= ' LIMIT ' . $pageSize;
		} else { // $page > 1
			$queryStatement .= ' LIMIT ' . ($pageSize * ($page - 1)) . ',' . $pageSize;
		}

		$queryStatement .= ';';
		
		$done($queryStatement, $queryParams);
	}	
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	static function validateObjectSearchParams($params) {
		global $g_AppErrors;
	
		if (!array_key_exists('object_id', $params) && !array_key_exists('name', $params) && !array_key_exists('tags', $params) && !array_key_exists('status', $params)) {
			return $g_AppErrors['MISSING_SUBJECT_SEARCH_PARAM'];
		}
		
		if (array_key_exists('items_per_page', $params) && $params['items_per_page'] != null) {
			$pageSize = intVal($params['items_per_page']);
			if ($pageSize < GEO_SUBJECT_SEARCH_RESULT_PAGE_SIZE_MIN || $pageSize > GEO_SUBJECT_SEARCH_RESULT_PAGE_SIZE_MAX) {
				return $g_AppErrors['INVALID_SUBJECT_SEARCH_PARAM_PAGE_SIZE'];
			}
		}

		if (array_key_exists('page', $params) && $params['page'] != null) {
			$page = intVal($params['page']);
			if ($page < 1) {
				return $g_AppErrors['INVALID_PARAM_PAGE_NUMBER'];
			}
		}

		return null;
	}

	/* Search object by name, tags, status, page_size, page. */
	public static function searchObject($searchParams, $done) {
		$error = self::validateObjectSearchParams($searchParams);

		if ($error == null) {
			if (array_key_exists('object_id', $searchParams)) {
				// search by Object Id
				$objectId = $_GET['object_id'];
				$object = self::findById($objectId);
				$done($object, null);
			} else {
				// search by other params
				self::buildObjectSearchSQLQuery($searchParams, function ($queryStatement, $queryParams) use ($done) {
					$rows = AppDB::$userDb->query($queryStatement, $queryParams);

					$result = [];
					foreach ($rows as $row) {
						$result[] = self::objectRowToJSON($row);
					}
					// $result = [ 'query' => $queryStatement, 'params' =>  $queryParams ];
					$done($result, null);
				});
			}
		} else {
			$done(null, $error);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	static function validateObjectSearchNearbyParams($params) {
		global $g_AppErrors;
	
		if (!array_key_exists('lat', $params) || !array_key_exists('lon', $params) || !array_key_exists('radius', $params)) {
			return $g_AppErrors['MISSING_SUBJECT_SEARCH_NEARBY_PARAM'];
		}
		
		if (!self::validateObjectLat($params['lat'])) {
			return $g_AppErrors['INVALID_LATITUDE'];
		}
		
		if (!self::validateObjectLon($params['lon'])) {
			return $g_AppErrors['INVALID_LONGITUDE'];
		}
		
		if (!self::validateDistance($params['radius'])) {
			return $g_AppErrors['INVALID_RADIUS'];
		}
		
		if (array_key_exists('result_limit', $params) && $params['result_limit'] != null) {
			$resultLimit = intVal($params['result_limit']);
			if ($resultLimit < 1 || $resultLimit > GEO_SUBJECT_SEARCH_NEARBY_RESULT_LIMIT_MAX) {
				return $g_AppErrors['INVALID_SUBJECT_SEARCH_PARAM_RESULT_LIMIT'];
			}
		}

		return null;
	}
	
	/* Search object by geolocation, tags, status. */
	public static function searchObjectNearby($searchParams, $done) {
		$error = self::validateObjectSearchNearbyParams($searchParams);

		if ($error == null) {
			self::buildObjectSearchSQLQuery($searchParams, function ($queryStatement, $queryParams) use ($searchParams, $done) {
				$rows = AppDB::$userDb->query($queryStatement, $queryParams);

				$lat = $searchParams['lat'];
				$lon = $searchParams['lon'];
				$radius = $searchParams['radius'];
				$radius_unit = null;
				$radius_value = null;
				self::parseDistanceString($radius, $radius_unit, $radius_value);
				$distanceRatio = 1.0;
				if ($radius_unit == 'm') {
					$distanceRatio = 1000.0;
				} if ($radius_unit == 'mi') {
					$distanceRatio = MeasurementUtil::kilometer2Mile(1.0);
				} if ($radius_unit == 'ft') {
					$distanceRatio = MeasurementUtil::Meter2Foot(1000.0);
				}
				$result = [];
				foreach ($rows as $row) {
					$object = self::objectRowToJSON($row);
					$object['approx_distance'] = GeocodeUtil::distanceInKm($lat, $lon, $row['lat'], $row['lon']) * $distanceRatio;
					$result[] = $object;
				}
				
				// Sort result by the distance
				$comparision = function($s1, $s2){
					return $s1['approx_distance'] > $s2['approx_distance'];
				};
				usort($result, $comparision);
				
				// Slice the result array to the result_limit param
				$resultLimit = array_key_exists('result_limit', $searchParams) ? intVal($searchParams['result_limit']) : GEO_SUBJECT_SEARCH_NEARBY_RESULT_LIMIT_DEFAULT;
				$result = array_slice($result, 0, $resultLimit);
				
				// $result = [ 'query' => $queryStatement, 'params' =>  $queryParams ];
				// $distance  = GeocodeUtil::distanceInKm(38.898556, -77.037852, 38.897147, -77.043934) * 1000;
				// $result = [ 'distance' => $distance];
				$done($result, null);
			});
		} else {
			$done(null, $error);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	/* Validate data before registering an object. */
	static function validateNewObject($object) {
		global $g_AppErrors;
	
		if (!array_key_exists('id', $object) || !self::validateObjectId($object['id'])) {
			return $g_AppErrors['MISSING_OR_INVALID_SUBJECT_ID'];
		}
	
		if (!array_key_exists('name', $object) || !self::validateObjectName($object['name'])) {
			return $g_AppErrors['MISSING_OR_INVALID_SUBJECT_NAME'];
		}

		if (array_key_exists('description', $object) && $object['description'] != null && !self::validateObjectDescription($object['description'])) {
			return $g_AppErrors['INVALID_SUBJECT_DESCRIPTION'];
		}

		if (array_key_exists('custom_data', $object) && $object['custom_data'] != null && !self::validateObjectCustomData($object['custom_data'])) {
			return $g_AppErrors['INVALID_SUBJECT_CUSTOM_DATA'];
		}
	
		if (array_key_exists('tags', $object) && $object['tags'] != null && !self::validateObjectTags($object['tags'])) {
			return $g_AppErrors['INVALID_SUBJECT_TAGS'];
		}

		if (array_key_exists('invisible', $object) && $object['invisible'] != null && !self::validateObjectInvisible($object['invisible'])) {
			return $g_AppErrors['INVALID_SUBJECT_INVISIBILITY'];
		}
	
		if (array_key_exists('status', $object) && $object['status'] != null && !self::validateObjectStatus($object['status'])) {
			return $g_AppErrors['INVALID_SUBJECT_STATUS'];
		}

		if (array_key_exists('location_data', $object) && $object['location_data'] != null) {
			$locationError = self::validateObjectLocation($object['location_data']);
			if ($locationError != null) {
				return $locationError;
			}
		}
	
		return null;
	}

	/* Register new object. */
	public static function registerObject($object) {
		$timestamp = new DateTime();
		$location = array_key_exists('location_data', $object) ? $object['location_data'] : null;
		$lat = $location != null && array_key_exists('lat', $location) ? $location['lat'] : null;
		$lon = $location != null && array_key_exists('lon', $location) ? $location['lon'] : null;
		$speed = $location != null && array_key_exists('speed', $location) ? $location['speed'] : null;
		$course = $location != null && array_key_exists('course', $location) ? $location['course'] : null;
		$context = $location != null && array_key_exists('context', $location) ? $location['context'] : null;
	
		AppDB::$userDb->query(
				'INSERT INTO object (id, name, `description`, lat, lon, speed, course, current_context, location_data_timestamp, tags, custom_data, invisible, status, date_added)
					VALUES (%?, %?, %?, %?, %?, %?, %?, %?, %?, %?, %?, %?, %?, %?)',
				$object['id'], $object['name'], 
				array_key_exists('description', $object) ? $object['description'] : null,
				$lat, $lon, $speed, $course, $context, $timestamp,
				array_key_exists('tags', $object) ? $object['tags'] : null,
				array_key_exists('custom_data', $object) ? $object['custom_data'] : null,
				array_key_exists('invisible', $object) && is_bool($object['invisible']) ? $object['invisible'] : false,
				array_key_exists('status', $object) ? $object['status'] : null,
				$timestamp
		);
		
		// add the location if needed
		if ($location != null) {
			// add timestamp if not exist
			if (!array_key_exists('timestamp', $location)) {
				$location['timestamp'] = $timestamp;
			}
			self::addObjectLocation($object['id'], $location);
		}
		
		if (array_key_exists('tags', $object) && $object['tags'] != null) {
			// add the tags
			$tags = explode(',', $object['tags']);
			self::addObjectTags($object['id'], $tags);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	/* Validate data before updating an object. */
	static function validateObjectForUpdate($object) {
		global $g_AppErrors;
	
		if (array_key_exists('name', $object) && $object['name'] != null && !self::validateObjectName($object['name'])) {
			return $g_AppErrors['MISSING_OR_INVALID_SUBJECT_NAME'];
		}

		if (array_key_exists('description', $object) && $object['description'] != null && !self::validateObjectDescription($object['description'])) {
			return $g_AppErrors['INVALID_SUBJECT_DESCRIPTION'];
		}

		if (array_key_exists('custom_data', $object) && $object['custom_data'] != null && !self::validateObjectCustomData($object['custom_data'])) {
			return $g_AppErrors['INVALID_SUBJECT_CUSTOM_DATA'];
		}
	
		if (array_key_exists('tags', $object) && $object['tags'] != null && !self::validateObjectTags($object['tags'])) {
			return $g_AppErrors['INVALID_SUBJECT_TAGS'];
		}

		if (array_key_exists('invisible', $object) && $object['invisible'] != null && !self::validateObjectInvisible($object['invisible'])) {
			return $g_AppErrors['INVALID_SUBJECT_INVISIBILITY'];
		}
	
		if (array_key_exists('status', $object) && $object['status'] != null && !self::validateObjectStatus($object['status'])) {
			return $g_AppErrors['INVALID_SUBJECT_STATUS'];
		}

		if (array_key_exists('location_data', $object) && $object['location_data'] != null) {
			$locationError = self::validateObjectLocation($object['location_data']);
			if ($locationError != null) {
				return $locationError;
			}
		}
	
		return null;
	}

	/* Update an object. */
	public static function updateObject($objectId, $object) {
		$timestamp = new DateTime();
		$updateStatement = 'UPDATE object SET date_updated=%?_date_updated';
		$updateValues = [ 'date_updated' => $timestamp ];
		$needUpdateObject = false;
	
		if (array_key_exists('name', $object)) {
			$updateStatement .= ',name=%?_name';
			$updateValues['name'] = $object['name'];
			$needUpdateObject = true;
		}
	
		if (array_key_exists('description', $object)) {
			$updateStatement .= ',description=%?_description';
			$updateValues['description'] = $object['description'];
			$needUpdateObject = true;
		}
	
		if (array_key_exists('tags', $object)) {
			$updateStatement .= ',tags=%?_tags';
			$updateValues['tags'] = $object['tags'];
			$needUpdateObject = true;
		}
	
		if (array_key_exists('custom_data', $object)) {
			$updateStatement .= ',custom_data=%?_custom_data';
			$updateValues['custom_data'] = $object['custom_data'];
			$needUpdateObject = true;
		}
	
		if (array_key_exists('status', $object)) {
			$updateStatement .= ',status=%?_status';
			$updateValues['status'] = $object['status'];
			$needUpdateObject = true;
		}
	
		if (array_key_exists('location_data', $object)) {
			if ($object['location_data'] == null) {
				$updateStatement .= ',lat=NULL,lon=NULL,speed=NULL,course=NULL,current_context=NULL,location_data_timestamp=NULL';
			} else {
				if (array_key_exists('lat', $object['location_data'])) {
					$updateStatement .= ',lat=%?_lat';
					$updateValues['lat'] = $object['location_data']['lat'];
				}
	
				if (array_key_exists('lon', $object['location_data'])) {
					$updateStatement .= ',lon=%?_lon';
					$updateValues['lon'] = $object['location_data']['lon'];
				}
	
				if (array_key_exists('speed', $object['location_data'])) {
					$updateStatement .= ',speed=%?_speed';
					$updateValues['speed'] = $object['location_data']['speed'];
				}
	
				if (array_key_exists('course', $object['location_data'])) {
					$updateStatement .= ',course=%?_course';
					$updateValues['course'] = $object['location_data']['course'];
				}
	
				if (array_key_exists('context', $object['location_data'])) {
					$updateStatement .= ',current_context=%?_current_context';
					$updateValues['current_context'] = ($object['location_data']['context']);
				}
				
				$updateStatement .= ',location_data_timestamp=%?_location_data_timestamp';
				$updateValues['location_data_timestamp'] = $timestamp;
			}
			$needUpdateObject = true;
		}
	
		if (array_key_exists('invisible', $object)) {
			$updateStatement .= ',invisible=%?_invisible';
			$updateValues['invisible'] = (is_bool($object['invisible']) ? $object['invisible'] : false);
			$needUpdateObject = true;
		}
	
		$updateStatement .= ' WHERE id=%?_object_id';
		$updateValues['object_id'] = $objectId;
	
		$updateStatement .= ';';

		if ($needUpdateObject) {
			AppDB::$userDb->query( $updateStatement, $updateValues );
		}

		if (array_key_exists('location_data', $object) && $object['location_data'] != null) {
			// add the location if needed
			$location = $object['location_data'];
			// add timestamp if not exist
			if (!array_key_exists('timestamp', $location)) {
				$location['timestamp'] = $timestamp;
			}
			self::addObjectLocation($objectId, $location);
		}
		
		if (array_key_exists('tags', $object) && $object['tags'] != null) {
			// need to update tags
			$tags = explode(',', $object['tags']);
			self::removeObjectTags($objectId); // remove old tags
			self::addObjectTags($objectId, $tags); // add new tags
		}
	}

	/* Update location data an object. */
	public static function updateObjectLocationData($objectId, $locationData) {
		$timestamp = array_key_exists('timestamp', $locationData) ? self::dateTimeFromISO8601String($locationData['timestamp']) : new DateTime();
		$updateStatement = 'UPDATE object SET ';
		$updateValues = [];
	
		if ($locationData == null) {
			$updateStatement .= 'lat=NULL,lon=NULL,speed=NULL,course=NULL,current_context=NULL,location_data_timestamp=NULL';
		} else {
			$updateStatement .= 'lat=%?_lat,lon=%?_lon,speed=%?_speed,course=%?_course,current_context=%?_current_context,location_data_timestamp=%?_timestamp';

			$updateValues['lat'] = array_key_exists('lat', $locationData) ? $locationData['lat'] : null;

			$updateValues['lon'] = array_key_exists('lon', $locationData) ? $locationData['lon'] : null;

			$updateValues['speed'] = array_key_exists('speed', $locationData) ? $locationData]['speed'] : null;

			$updateValues['course'] = array_key_exists('course', $locationData) ? $locationData['course'] : null;

			$updateValues['current_context'] = array_key_exists('context', $locationData) ? $locationData['context'] : null;
			
			$updateValues['location_data_timestamp'] = $timestamp;
		}
	
		$updateStatement .= ' WHERE id=%?_object_id';
		$updateValues['object_id'] = $objectId;
	
		$updateStatement .= ';';

		AppDB::$userDb->query( $updateStatement, $updateValues );
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	/* Remove an object. */
	public static function removeObject($objectId) {
		AppDB::$userDb->query( 
			'DELETE from object WHERE id=%?;', $objectId
		);
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	static function createObjectLocationRow($objectId, $location) {
		$timestamp = array_key_exists('timestamp', $location) ? $location['timestamp'] : new DateTime();
		$speed = array_key_exists('speed', $location) ? $location['speed'] : null;
		$course = array_key_exists('course', $location) ? $location['course'] : null;
		$context = array_key_exists('context', $location) ? $location['context'] : null;
		$row = [
			'object_id' => $objectId,
			'lat' => $location['lat'],
			'lon' => $location['lon'],
			'context' => $context,
			'speed' => $speed,
			'course' => $course,
			'timestamp' => $timestamp,
		];
		return $row;
	}
	
	/* Add an object's location record. */
	public static function addObjectLocation($objectId, $location) {
		$row = self::createObjectLocationRow($objectId, $location);
		AppDB::$userDb->insert('object_location', $row);

		$message = json_encode([
			'event' => 'NEW_LOCATION',
			'object_id' => $objectId,
			'location' => $location
		]);
		AppPubSub::publish('object_location', $message);
	}

	/* Add an object's multiple location records. */
	public static function addObjectLocationArray($objectId, $locations) {
		$rows = [];
		foreach ($locations as $loc) {
			$rows[] = self::createObjectLocationRow($objectId, $loc);
		}
		AppDB::$userDb->insert('object_location', $rows);
		
		$message = json_encode([
			'event' => 'NEW_LOCATIONS',
			'object_id' => $objectId,
			'locations' => $locations
		]);
		AppPubSub::publish('object_location', $message);
	}

	/* Remove an object's location history. */
	public static function removeObjectLocationHistory($objectId) {
		AppDB::$userDb->query(
			'DELETE from object_location WHERE object_id=%?;', $objectId
		);
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	static function validateObjectLocationSearchParams($params) {
		global $g_AppErrors;
		if (!array_key_exists('object_id', $params)) {
			return $g_AppErrors['MISSING_OR_INVALID_SUBJECT_ID'];
		}
		
		if (array_key_exists('items_per_page', $params) && $params['items_per_page'] != null) {
			$pageSize = intVal($params['items_per_page']);
			if ($pageSize > GEO_SUBJECT_LOCATION_SEARCH_RESULT_PAGE_SIZE_MAX) {
				return $g_AppErrors['INVALID_SUBJECT_LOCATION_SEARCH_PARAM_PAGE_SIZE'];
			}
		}

		if (array_key_exists('page', $params) && $params['page'] != null) {
			$page = intVal($params['page']);
			if ($page < 1) {
				return $g_AppErrors['INVALID_PARAM_PAGE_NUMBER'];
			}
		}
	}
	
	// Get object location history
	public static function searchObjectLocation($searchParams, $done) {
		$error = self::validateObjectLocationSearchParams($searchParams);

		if ($error == null) {
			$page = array_key_exists('page', $searchParams) ? intVal($searchParams['page']) : 1;
			$pageSize = array_key_exists('items_per_page', $searchParams) ? intVal($searchParams['items_per_page']) : GEO_SUBJECT_LOCATION_SEARCH_RESULT_PAGE_SIZE_DEFAULt;
			
			$queryStatement = 'SELECT lat, lon, speed, course, context, `timestamp` FROM object_location WHERE object_id=%?_object_id ORDER BY `timestamp` DESC ';
			
			if ($page == 1) {
				$queryStatement .= ' LIMIT ' . $pageSize;
			} else { // $page > 1
				$queryStatement .= ' LIMIT ' . ($pageSize * ($page - 1)) . ',' . $pageSize;
			}
			
			$queryStatement .= ';';
			
			$queryParams = [
				'object_id' => $searchParams['object_id']
			];
			
			$rows = AppDB::$userDb->query($queryStatement, $queryParams);
			$result = [];
			foreach ($rows as $row) {
				$location = [
					'lat' => floatval($row['lat']), 
					'lon' => floatval($row['lon']), 
					'speed' => floatval($row['speed']), 
					'course' => floatval($row['course']), 
					'context' => $row['context'],
					'timestamp' => date('c', strtotime($row['timestamp']))
				];
				$result[] = $location;
			}
			// $result = [ 'query' => $queryStatement, 'params' =>  $queryParams ];
			$done($result, null);
		} else {
			$done(null, $error);
		}
	}
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	/* Add tags of an object */
	public static function addObjectTags($objectId, $tags) {
		$rows = [];
		foreach ($tags as $tag) {
			$rows[] = [
				'object_id' => $objectId,
				'tag' => $tag
			];
		}
		AppDB::$userDb->insert('object_tag', $rows);
	}
	
	/* Remove the tags of an object */
	public static function removeObjectTags($objectId) {
		AppDB::$userDb->query(
			'DELETE from object_tag WHERE object_id=%?;', $objectId
		);
	}
	
}


?>
