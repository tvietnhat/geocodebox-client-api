<?php

require_once ( dirname(dirname(__FILE__)) . '/libs/Redis/class.php' );

class AppPubSub {
	public static $redisCli = null;
	
	public static function init() {
		$redisConfig = include (dirname(dirname(__FILE__)) . '/config/redis.php');
		$pubsubConfig = $redisConfig['PUBSUB'];
		self::$redisCli = new redis_cli($pubsubConfig['host'], $pubsubConfig['port']);
	}
	
	public static function publish($channel, $message) {
		self::$redisCli->cmd('PUBLISH', $channel, $message) -> set();
	}
}

AppPubSub::init();

?>