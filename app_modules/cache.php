<?php

require_once ( dirname(dirname(__FILE__)).'/libs/SimpleCache/cache.class.php' );
require_once ( dirname(dirname(__FILE__)).'/libs/Redis/class.php' );


class RedisCache extends redis_cli {
	public function __construct($config) {
		parent::__construct($config['host'], $config['port']);
	}
	
	public function store($key, $value, $expireInSeconds = 0) {
		if ($expireInSeconds != 0) {
			$response = $this -> cmd('SETEX', $key, $expireInSeconds, $value) -> set();
		} else {
			$response = $this -> cmd('SET', $key, $value) -> set();
		}
		return ($response === 'OK');
	}
	
	public function retrieve($key) {
		$response = $this -> cmd('GET', $key) -> get();
		return $response;
	}
}



class AppCache {

	public static $authCache = null;
	public static $objectCache = null;

	public static function init() {
		self::$authCache = new Cache('auth');

		$redisConfig = include dirname(dirname(__FILE__)) . '/config/redis.php';
		self::$objectCache = new RedisCache($redisConfig['CACHE']);
	}
}

AppCache::init();

?>
