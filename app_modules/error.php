<?php

$g_AppErrors = [
	'UNKNOWN_ERROR' => [
		'code' => 1001, 
		'description' => 'Unknown error.' 
	],
	
	'USER_ACCOUNT_LOCKED' => [ 
		'code' => 1002, 
		'description' => 'Account locked.' 
	],

	'INVALID_USERNAME_OR_PASSWORD' => [ 
		'code' => 1003, 
		'description' => 'Invalid username or password.' 
	],

	'MISSING_OR_INVALID_ACCESS_TOKEN' => [ 
		'code' => 1004, 
		'description' => 'Missing or invalid access token.' 
	],

	'SUBJECT_NOT_FOUND' => [ 
		'code' => 1005, 
		'description' => 'Object not found.' 
	],

	'INVALID_SEARCH_PARAMS' => [ 
		'code' => 1006, 
		'description' => 'Invalid search param.'
	],

	'MISSING_OR_INVALID_SUBJECT_ID' => [ 
		'code' => 1007, 
		'description' => 'Missing or invalid Object Id. Object Id must not be null or empty, and it must not contain more than 36 characters.'
	],

	'MISSING_OR_INVALID_SUBJECT_NAME' => [ 
		'code' => 1008, 
		'description' => 'Object Name must not be null or empty, and it must not contain more than 64 characters.'
	],

	'INVALID_SUBJECT_CATEGORY' => [ 
		'code' => 1009, 
		'description' => 'Object Category must be a string and must not contain more than 45 characters.'
	],

	'INVALID_SUBJECT_CUSTOM_DATA' => [ 
		'code' => 1010, 
		'description' => 'Object Custom Data must be a string and must not contain more than 1024 characters.'
	],

	'INVALID_LATITUDE' => [ 
		'code' => 1011, 
		'description' => 'Latitude must be a number and must be between -90.0 and 90.0 degree.'
	],

	'INVALID_LONGITUDE' => [ 
		'code' => 1012, 
		'description' => 'Longitude must be a number and must be between -180.0 and 180.0 degree.'
	],

	'SUBJECT_DUPLICATE_WHEN_REGISTER' => [ 
		'code' => 1013, 
		'description' => 'Object with the same Id is found. Please choose another Object Id.'
	],

	'INVALID_SUBJECT_INVISIBILITY' => [ 
		'code' => 1014, 
		'description' => 'Object Invisible must be a boolean.'
	],

	'INVALID_SUBJECT_LOCATION_CONTEXT' => [ 
		'code' => 1015, 
		'description' => 'Object Location Context must be a string and must not contain more than 255 characters.'
	],
	
	'INVALID_SUBJECT_TAGS' => [ 
		'code' => 1016, 
		'description' => 'Object Tags must be a list of comma-separated, unique keywords. It must not contain more than 10 keywords. The length of each keyword must not exceed 45 characters.'
	],

	'INVALID_SUBJECT_DESCRIPTION' => [ 
		'code' => 1017, 
		'description' => 'Object Description must be a string and must not contain more than 255 characters.'
	],
	
	'INVALID_SUBJECT_STATUS' => [ 
		'code' => 1018, 
		'description' => 'Object Status must be a string and must not contain more than 45 characters.'
	],
	
	'INVALID_SUBJECT_SEARCH_PARAM_PAGE_SIZE' => [ 
		'code' => 1019, 
		'description' => 'Number of items per page must be between 5 and 100.'
	],
	
	'MISSING_SUBJECT_SEARCH_PARAM' => [ 
		'code' => 1020, 
		'description' => 'At least one of following parameters needed for searching objects: name, tags, status.'
	],
	
	'MISSING_SUBJECT_SEARCH_NEARBY_PARAM' => [ 
		'code' => 1021, 
		'description' => 'The following parameters are needed for searching objects by geolocation: lat, lon, radius.'
	],
	
	'INVALID_RADIUS' => [ 
		'code' => 1022, 
		'description' => 'Radius must be measured in metres(m), kilometres(km), feet(ft) or miles(mi). Sample values: 200m, 2.4km, 500ft, 1.8mi.'
	],
	
	'INVALID_PARAM_PAGE_NUMBER' => [ 
		'code' => 1023, 
		'description' => 'Page number must be greater than or equal to 1.'
	],
	
	'INVALID_SUBJECT_SEARCH_PARAM_RESULT_LIMIT' => [ 
		'code' => 1024, 
		'description' => 'Result limit must be between 1 and 100.'
	],
	
	'INVALID_SUBJECT_LOCATION_SEARCH_PARAM_PAGE_SIZE' => [ 
		'code' => 1025, 
		'description' => 'Number of items per page must be between 1 and 100.'
	],
	
	'INVALID_API_KEY' => [ 
		'code' => 1026, 
		'description' => 'Invalid api key.'
	],
	
	'INVALID_LOCATION_DATA_TIMESTAMP' => [ 
		'code' => 1027, 
		'description' => 'Invalid location data timestamp. ISO 8601 date format required.'
	],
	
	'INVALID_LOCATION_DATA_SPEED' => [ 
		'code' => 1028, 
		'description' => 'Invalid value of speed. Speed must be a number.'
	],
	
	'INVALID_LOCATION_DATA_COURSE' => [ 
		'code' => 1028, 
		'description' => 'Invalid value of course. Course must be a number.'
	],
	
];

	
?>

