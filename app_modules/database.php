<?php

require_once dirname(dirname(__FILE__)) . '/libs/MeekroDB/db.class.php';

$dbConfig = include dirname(dirname(__FILE__)) . '/config/database.php';


class AppDB {

	public static $userDb = null;

	public static $adminDb = null;

	public static function databaseWithConfig($config) {
		$db = new MeekroDB(
			$config["host"],
			$config["user"],
			$config["password"],
			$config["database"],
			$config["port"],
			$config["encoding"]
		);
		return $db;
	}

	public static function init() {
		global $dbConfig;
		self::$adminDb = self::databaseWithConfig($dbConfig['ADMIN_DB_CONFIG']);
	}
	
	public static function initUserDbWithConfig($config) {
		self::$userDb = self::databaseWithConfig($config);
	}
}

AppDB::init();

?>
