<?php

require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	$error = null;
	
	if ($authInfo != null) {
		$responseObj = [ 'auth_info' => $authInfo ];
	} else {
		$error = $authError;
	}
	
	if ($error != null) {
		$responseObj = [ 'error' => $error];
	}
	
	header('Content-Type: application/json');
	echo json_encode($responseObj);
});



?>
