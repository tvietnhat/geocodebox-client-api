<?php

require_once 'app_modules/auth.php';

$username = $_GET['username'];
$password = $_GET['password'];
$apiKey = $_GET['api_key'];

AppAuth::authenticate($username, $password, $apiKey, function ($authInfo, $error){
	if ($error) {
	    $responseObj = ['error' => $error];
	} else {
		$token = $authInfo['token'];
	    $responseObj = [ 'access_token' => $token ];
	}

	header('Content-Type: application/json');
	echo json_encode($responseObj);
});


?>

