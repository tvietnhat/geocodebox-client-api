#GeocodeBox API Web Service


##Overview
  Our API web service only supports JSON at this stage. All data passed in request body and returned in the response is in JSON. Setting `Content-Type` to `application/json; charset=utf-8` in your request header is nice to have and not compulsory. 
  
  The entry point of this API is the Authenticate Service. This service issues an access token which later can be used for calling other protected services in this API.

#### Error Response:
  In some cases such as passing an invalid parameter to an API Service, you may receive an error in the response. An error response includes an error dictionary with a code and a description. Below is an example:
```
{
	"error": {
		"code": 1003,
		"description": "Invalid username or password."
	}
}
```

##List of API Services
* [Authenticate][1]
* [Register an object][2]
* [Update an object][4]
* [Report location of an object][6]
* [Retrieve information of an object][3]
* [Retrieve location of an object][7]
* [Search objects][9]
* [Search objects nearby][10]
* [Remove an object][5]

[1]: #authenticate
[2]: #register-object
[3]: #retrieve-object
[4]: #update-object
[5]: #remove-object
[6]: #report-object-location
[7]: #retrieve-object-location
[8]: #clear-object-location
[9]: #search-objects
[10]: #search-objects-nearby

<a id='authenticate'/>
###Authenticate 
  You need to authenticate to get an access token before using it in other secured API services.

* **Method:** `GET`

* **URL:** `/authenticate?api_key=[string]&username=[string]&password=[string]`

  **Query String Params:**
 
  **`api_key`**: **Required**. Each project is allocated with an API key. Please go to your project portal to get the API key.

  **`username`**: **Required**. Your account email address.

  **`password`**: **Required**. Your account password.

  **Sample URL:** `https://your-project.geocodebox.com/api/authenticate?api_key=0123456789&username=email@domain.com&password=mypassword`

* **Success Response:**
  
  _A success response includes an access token which can be used for other secured API calls._
  **Sample success response:** 
```
	{ 
		"access_token" : "sample-access-token-0123456789" 
	}
```


---
<a id='register-object'/>
###Register an object
  Registering an object.

* **Method:** `POST`

* **URL:** `/object/register?access_token=[string]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **Sample URL:** `https://your-project.geocodebox.com/api/object/register?access_token=0123456789`

* **Request Body:**
  A dictionary of object properties must be passed in the request body.

  **`id`**: **_Required_**. A string identifying your object. It must be duplicate with any other object that you have registered with the API. must not be null or empty, and it must not contain more than 36 characters.
  
  **`name`**: **_Required_**. The name of the object. Must not be null or empty. Must not contain more than 64 characters.
  
  **`tags`**: _Optional_. Object Tags is a list of comma-separated, unique keywords. It must not contain more than 10 keywords. The length of each keyword must not exceed 45 characters. You can use this field to categorize your objects.
  
  **`location_data`**: _Optional_. A dictionary containing geolocation-based information of latitude, longitude, speed, course, context.
  
  **`lat`**: _Optional_. Latitude of the location. Measured in degrees. Must be a decimal number between -90.0 and 90.0. Sample values: 33.94829832, -25.48248239.
  
  **`lon`**: _Optional_. Longitude of the location. Measured in degrees. Must be a decimal number between -180.0 and 180.0. Sample values: 143.98329482, -115.42398248.
  
  **`speed`**: _Optional_. Speed of the object. Measured in meters per second. Must be a number. Sample values: 10, 15.3.
  
  **`course`**: _Optional_. Heading direction of the object. Measured in degrees. Range: 0.0 - 360.0, 0 being true North.
  
  **`context`**: _Optional_. Additional data logged at the time the location cordinate is recorded.
  
  **`timestamp`**: _Optional_. Timestamp of the location data. In ISO-8601 format. If `null` or ignored, the API will use its current server time.

  **Sample Request Body:**
```
	{
		"id": "abcd1234",
		"name": "John's Food Truck",
		"tags": "car,business,food",
		"location_data": {
			"lat": 39.0,
			"lon": -150.0,
			"speed": 39.0,
			"course": 50.0,
			"context": "some context data",
			"timestamp": "2016-01-06T16:29:22+11:00"
		},
		"status": "active"
	}
```

* **Success Response:**
  
  _A success response includes a boolean field `success` which indicates the object has been added successfully._
  **Sample success response:** 
```
	{ 
		"success" : true 
	}
```

---
<a id='update-object'/>
###Update an object 
  Updating information of an object.

* **Method:** `POST`

* **URL:** `/object/update?access_token=[string]&object_id=[string]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **`object_id_`**: **_Required_**. Object ID.

  **Sample URL:** `https://your-project.geocodebox.com/api/object/update?access_token=0123456789&object_id=abcd1234`

* **Request Body:**
  
  A dictionary of object properties must be passed in the request body.
  
  **Please note:** Assigning a property to null is different to not assigning it a value. For example, setting `{ "tags" : null }` will cause the API to update the value of `tags` to `null`. For safety, only pass the values of the object properties you want to update.

  **`name`**: _Optional_. The name of the object. Must not be null or empty. Must not contain more than 64 characters.
  
  **`tags`**: _Optional_. Object Tags is a list of comma-separated, unique keywords. It must not contain more than 10 keywords. The length of each keyword must not exceed 45 characters. You can use this field to categorize your objects.
  
  **`location_data`**: _Optional_. A dictionary containing geolocation-based information of latitude, longitude, speed, course, context.
  
  **`lat`**: _Optional_. Latitude of the location. Measured in degrees. Must be a decimal number between -90.0 and 90.0. Sample values: 33.94829832, -25.48248239.
  
  **`lon`**: _Optional_. Longitude of the location. Measured in degrees. Must be a decimal number between -180.0 and 180.0. Sample values: 143.98329482, -115.42398248.
  
  **`speed`**: _Optional_. Speed of the object. Measured in meters per second. Must be a number. Sample values: 10, 15.3.
  
  **`course`**: _Optional_. Heading direction of the object. Measured in degrees. Range: 0.0 - 360.0, 0 being true North.
  
  **`context`**: _Optional_. Additional data logged at the time the location cordinate is recorded.
  
  **`timestamp`**: _Optional_. Timestamp of the location data. In ISO-8601 format. If `null` or ignored, the API will use its current server time.

  **Sample Request Body:**
  
  Update object's name
```
	{
		"name": "Tony's Food Truck"
	}
```
  
  Update multiple properties of an object
```
	{
		"name": "John's Food Truck",
		"tags": "car,business,transport",
		"location_data": {
			"lat": 39.0,
			"lon": -150.0,
			"speed": 39.0,
			"course": 50.0,
			"context": "some context data",
			"timestamp": "2016-01-06T16:29:22+11:00"
		},
		"status": "active"
	}
```

* **Success Response:**
  
  _A success response includes a boolean field `success` which indicates the object has been added successfully._
  **Sample success response:** 
```
	{ 
		"success" : true 
	}
```


---
<a id='retrieve-object'/>
###Retrieve information of an object
  Get detail information including the current location data of an object.

* **Method:** `GET`

* **URL:** `/object?access_token=[string]&object_id=[string]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **`object_id`**: **_Required_**. Object ID.

  **Sample URL:** `https://your-project.geocodebox.com/api/object?access_token=0123456789&object_id=abcd1234`

* **Success Response:**
  
  _A success response is a dictionary of object properties._
  
  **Sample success response:** 
```
	{
		"id": "abcd1234",
		"name": "John's Food Truck",
		"description": "",
		"location_data": {
			"lat": -33.831477038396,
			"lon": 151.24425532216,
			"speed": -1,
			"course": -1,
			"context": "{\"horizontalAccuracy\":107.9122785379468,\"verticalAccuracy\":112.1039469504754}",
			"timestamp": "2016-01-07T10:43:59+11:00"
		},
		"tags": "car,business,transport",
		"custom_data": null,
		"date_added": "2015-09-01T01:58:17+10:00",
		"status": "active"
	}
```


---
<a id='search-objects'/>
###Search objects
  Searching objects by its name, tags, status, etc except its location.

* **Method:** `GET`

* **URL:** `/object/search?access_token=[string]&name=[string]&tags=[string]&status=[string]&items_per_page=[integer]&page=[integer]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **`name`**: _Optional_. Object Name.

  **`tags`**: _Optional_. Object Tags. A comma-separated string.

  **`status`**: _Optional_. Object Status.
  
  **`items_per_page`**: _Optional_. As integer indicating number of objects returned in a search request. Maximum value is 100. Default value is 30.

  **`page`**: _Optional_. Page number. Starting from 1.

  **Sample URL:** 

  Search by name: `https://your-project.geocodebox.com/api/object/search?access_token=0123456789&name=food+truck`
  
  Search by tags: `https://your-project.geocodebox.com/api/object/search?access_token=0123456789&tags=restaurant,Italian`

  Search by tags and status with pagination: `https://your-project.geocodebox.com/api/object/search?access_token=0123456789&tags=restaurant,Italian&status=active&items_per_page=20&page=2`

* **Success Response:**
  
  _A success response is an array of dictionaries of object properties._

  **Sample success response:** 
```
	[
		{
			"id": "0060abcc-4a1e-11e5-832d-001999e07ae6",
			"name": "Kiwi 1004",
			"description": null,
			"location_data": {
				"lat": 89.00000174297,
				"lon": -0.49995825703,
				"speed": null,
				"course": null,
				"context": null,
				"timestamp": "1970-01-01T10:00:00+10:00"
			},
			"tags": "restaurant",
			"custom_data": null,
			"date_added": "2015-09-01T01:58:17+10:00",
			"status": "inactive"
		}, 
		{
			"id": "0060bdb0-4a1e-11e5-832d-001999e07ae6",
			"name": "Kiwi 1009",
			"description": null,
			"location_data": {
				"lat": 89.000003653766,
				"lon": -0.499906346234,
				"speed": null,
				"course": null,
				"context": null,
				"timestamp": "1970-01-01T10:00:00+10:00"
			},
			"tags": "restaurant,bar",
			"custom_data": null,
			"date_added": "2015-09-01T01:58:17+10:00",
			"status": "active"
		}, 
		{
			"id": "0060f230-4a1e-11e5-832d-001999e07ae6",
			"name": "Kiwi 1024",
			"description": null,
			"location_data": {
				"lat": 89.000008621086,
				"lon": -0.499751378914,
				"speed": null,
				"course": null,
				"context": null,
				"timestamp": "1970-01-01T10:00:00+10:00"
			},
			"tags": "restaurant",
			"custom_data": null,
			"date_added": "2015-09-01T01:58:17+10:00",
			"status": "active"
		}
	]
```

---
<a id='search-objects-nearby'/>
###Search objects nearby
* **Method:** `GET`

* **URL:** `/object/nearby?access_token=[string]&lat=[number]&lon=[number]&radius=[string]&tags=[string]&status=[string]&result_limit=[integer]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **`lat`**: **_Required_**. Latitude of the location. Measured in degrees. Must be a decimal number between -90.0 and 90.0. Sample values: 33.94829832, -25.48248239.
  
  **`lon`**: **_Required_**. Longitude of the location. Measured in degrees. Must be a decimal number between -180.0 and 180.0. Sample values: 143.98329482, -115.42398248.
  
  **`radius`**: **_Required_**. Radius of the circle area to search for. Measured in meters or kilometers. Sample values: 200m, 2.5km.

  **`tags`**: _Optional_. Object Tags. A comma-separated string.

  **`status`**: _Optional_. Object Status.
  
  **`result_limit_`**: _Optional_. As integer indicating number of objects returned in a search request. Maximum value is 100. Default value is 10.

  **Sample URL:** 

  Search nearby location: `https://your-project.geocodebox.com/api/object/search/nearby?access_token=0123456789&lat=-34.374639&lon=151.707361&radius=500m`
  
  Search nearby with tags: `https://your-project.geocodebox.com/api/object/search/nearby?access_token=0123456789&lat=-34.374639&lon=151.707361&radius=500m&tags=restaurant`

  Search nearby with tags and result limit: `https://your-project.geocodebox.com/api/object/search/nearby?access_token=0123456789&lat=-34.374639&lon=151.707361&radius=500m&tags=restaurant&result_limit=50`

* **Notes:**

  _This search request returns maximum 100 objects. If there are more than 100 objects which satisfying a location-based search criteria, this API service doesn't guarantee the result contain the nearest objects. We suggest you to refine your search criteria for more accurate results._

* **Success Response:**
  
  _A success response is an array of dictionaries of object properties and the approximate distance to the geolocation specified in the search query. The approximate distance is measured in the unit specified in the `radius parameter. For example, if the `radius` is 300m the approx distance returned in the search result will be measured in meters._

  **Sample success response:** 
```
	[
		{
			"id": "58fac240-4a0a-11e5-832d-001999e07ae6",
			"name": "Apple 100654",
			"description": null,
			"location_data": {
				"lat": -34.37483875976,
				"lon": 151.70410224024,
				"speed": null,
				"course": null,
				"context": null,
				"timestamp": "1970-01-01T10:00:00+10:00"
			},
			"tags": "restaurant",
			"custom_data": null,
			"date_added": "2015-09-01T01:57:59+10:00",
			"status": "inactive",
			"approx_distance": 316.27150046072
		}, 
		{
			"id": "7f2f32ca-4a0a-11e5-832d-001999e07ae6",
			"name": "Mango 100654",
			"description": null,
			"location_data": {
				"lat": -34.37483937725,
				"lon": 151.70410162275,
				"speed": null,
				"course": null,
				"context": null,
				"timestamp": "1970-01-01T10:00:00+10:00"
			},
			"tags": "restaurant",
			"custom_data": null,
			"date_added": "2015-09-01T01:58:17+10:00",
			"status": null,
			"approx_distance": 316.32807536441
		}, 
		{
			"id": "2a8bfec6-4a17-11e5-832d-001999e07ae6",
			"name": "Coconut 50824",
			"description": null,
			"location_data": {
				"lat": -34.374837479028,
				"lon": 151.70404352097,
				"speed": null,
				"course": null,
				"context": null,
				"timestamp": "1970-01-01T10:00:00+10:00"
			},
			"tags": "restaurant",
			"custom_data": null,
			"date_added": "2015-09-01T01:58:17+10:00",
			"status": "active",
			"approx_distance": 321.66248279493
		}
	]
```  


---
<a id='report-object-location'/>
###Report location of an object
  This API service let you log a coordinate and other location-related data of an object. You can log one or multiple location-based data records in a single request.

* **Method:** `POST`

* **URL:** `/object/location/log?access_token=[string]&object_id=[string]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **`object_id`**: **_Required_**. Object ID.

  **Sample URL:** `https://your-project.geocodebox.com/api/object/location/log?access_token=0123456789&object_id=abcd1234`
  
* **Request Body:**
  
  A dictionary of location data properties must be passed in the request body. If you want to log batch of records in one request, just pass an array of dictionaries.
  
  **`lat`**: **_Required_**. Latitude of the location. Measured in degrees. Must be a decimal number between -90.0 and 90.0. Sample values: 33.94829832, -25.48248239.
  
  **`lon`**: **_Required_**. Longitude of the location. Measured in degrees. Must be a decimal number between -180.0 and 180.0. Sample values: 143.98329482, -115.42398248.
  
  **`speed`**: _Optional_. Speed of the object. Measured in meters per second. Must be a number. Sample values: 10, 15.3.
  
  **`course`**: _Optional_. Heading direction of the object. Measured in degrees. Range: 0.0 - 360.0, 0 being true North.
  
  **`context`**: _Optional_. Additional data logged at the time the location cordinate is recorded.
  
  **`timestamp`**: _Optional_. Timestamp of the location data. In ISO-8601 format. If `null` or ignored, the API will use its current server time.

  **Sample Request Body:**
  
  For a single location data record
```
	{
		"lat": -33.836582917057,
		"lon": 151.23435175686,
		"speed": 12,
		"course": 65.9,
		"context": "{\"horizontalAccuracy\":65,\"verticalAccuracy\":10}",
		"timestamp" : "2015-09-01T01:58:17+10:00"
	}
```
  
  For a batch of location data records
```
	[
		{
			"lat": -33.836582917057,
			"lon": 151.23435175686,
			"speed": 11,
			"course": -1,
			"context": "{\"horizontalAccuracy\":65,\"verticalAccuracy\":10}",
			"timestamp" : "2015-09-01T01:58:17+10:00"
		}, 
		{
			"lat": -33.836573020885,
			"lon": 151.23434552739,
			"speed": 12,
			"course": -1,
			"context": "{\"horizontalAccuracy\":65,\"verticalAccuracy\":10}",
			"timestamp" : "2015-09-01T01:58:27+10:00"
		}, 
		{
			"lat": -33.836523736859,
			"lon": 151.23434227479,
			"speed": 16,
			"course": -1,
			"context": "{\"horizontalAccuracy\":65,\"verticalAccuracy\":10}",
			"timestamp" : "2015-09-01T01:58:37+10:00"
		}
	]
```


---
<a id='retrieve-object-location'/>
###Retrieve location data of an object
  This service provides location data logged earlier of an object.
  
* **Method:** `GET`

* **URL:** `/object/location?access_token=[string]&object_id=[string]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **`object_id`**: **_Required_**. Object ID.

  **Sample URL:** `https://your-project.geocodebox.com/api/object/location?access_token=0123456789&object_id=abcd1234`

* **Success Response:**
  
  _A success response is an array of dictionaries of object's location data._
  
  **Sample success response:** 
```
	[
		{
			"lat": -33.831477038396,
			"lon": 151.24425532216,
			"speed": -1,
			"course": -1,
			"context": "{\"horizontalAccuracy\":107.9122785379468,\"verticalAccuracy\":112.1039469504754}",
			"timestamp": "2016-01-07T10:43:58+11:00"
		}, 
		{
			"lat": -33.831477038396,
			"lon": 151.24425532216,
			"speed": -1,
			"course": -1,
			"context": "{\"horizontalAccuracy\":107.9122785379468,\"verticalAccuracy\":112.1039469504754}",
			"timestamp": "2016-01-07T10:43:58+11:00"
		}, 
		{
			"lat": -33.833832878175,
			"lon": 151.23960739511,
			"speed": -1,
			"course": -1,
			"context": "{\"horizontalAccuracy\":65,\"verticalAccuracy\":27.65713716797393}",
			"timestamp": "2016-01-07T10:31:15+11:00"
		}
	]
```


---
<a id='remove-object'/>
###Remove an object
  Remove an object and its tags, location data permanently. 

* **Method:** `POST`

* **URL:** `/object/remove?access_token=[string]&object_id=[string]`

  **Query String Params:**

  **`access_token`**: **_Required_**. A security token issued by the Authentication Service.

  **`object_id`**: **_Required_**. Object ID.

  **Sample URL:** `https://your-project.geocodebox.com/api/object/remove?access_token=0123456789&object_id=abcd1234`
  
* **Success Response:**
  
  _A success response includes a boolean field `success` which indicates the object has been added successfully._
  
  **Sample success response:** 
```
	{ 
		"success" : true 
	}
```



