<?php

require_once 'app_modules/geo-object.php';
require_once 'app_modules/auth.php';


$authInfo = AppAuth::authenticateRequest(function ($authInfo, $authError){
	$responseObj = []; // default to no result
	$error = null;
	
	if ($authInfo != null) {
		$object = json_decode(file_get_contents("php://input"), true);
		$error = GeoObject::validateNewObject($object);
		if ($error == null) {
			$existingObject = GeoObject::findById($object['id']);
			global $g_AppErrors;

			if ($existingObject != null) {
				$error = $g_AppErrors['SUBJECT_DUPLICATE_WHEN_REGISTER'];
			} else {
				// do the registration
				GeoObject::registerObject($object);
				$responseObj = [ 'success' => true ];
			}
		}
	} else {
		$error = $authError;
	}
	
	if ($error != null) {
		$responseObj = [ 'error' => $error];
	}
	
	header('Content-Type: application/json');
	echo json_encode($responseObj);
});



?>
